%define _build_id_links none

Name:      compat-libicu50
Version:   50.2
Release:   3%{?dist}
Summary:   Compat package with icu libraries

License:   MIT and UCD and Public Domain
URL:       http://site.icu-project.org/

BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: doxygen, autoconf, python2

Source0:   https://github.com/unicode-org/icu/releases/download/release-50-2/icu4c-50_2-src.tgz
# According to ICU the layout "patch" should be applied to all versions earlier than 51.2
# See also http://site.icu-project.org/download/51#TOC-Known-Issues
Source1:   http://download.icu-project.org/files/icu4c/51.1/icu-51-layout-fix-10107.tgz
Source2:   icu-config.sh
Source10:   http://source.icu-project.org/repos/icu/data/trunk/tzdata/icunew/2018e/44/metaZones.txt
Source11:   http://source.icu-project.org/repos/icu/data/trunk/tzdata/icunew/2018e/44/timezoneTypes.txt
Source12:   http://source.icu-project.org/repos/icu/data/trunk/tzdata/icunew/2018e/44/windowsZones.txt
Source13:   http://source.icu-project.org/repos/icu/data/trunk/tzdata/icunew/2018e/44/zoneinfo64.txt
BuildRequires: doxygen, autoconf, python2

Patch1: icu.8198.revert.icu5431.patch
Patch2: icu.8800.freeserif.crash.patch
Patch3: icu.7601.Indic-ccmp.patch
Patch4: icu.9948.mlym-crash.patch
Patch5: gennorm2-man.patch
Patch6: icuinfo-man.patch
Patch7: icu.10143.memory.leak.crash.patch
Patch8: icu.10318.CVE-2013-2924_changeset_34076.patch
Patch9: icu.rhbz1074549.CVE-2013-5907.patch

# Explicitly conflict with older icu packages that ship libraries
# with the same soname as this compat package
#Conflicts: libicu < 64

%description
Compatibility package with icu libraries ABI version 50.


%prep
%setup -q -n icu
%setup -q -n icu -T -D -a 1
%patch1 -p2 -R -b .icu8198.revert.icu5431.patch
%patch2 -p1 -b .icu8800.freeserif.crash.patch
%patch3 -p1 -b .icu7601.Indic-ccmp.patch
%patch4 -p1 -b .icu9948.mlym-crash.patch
%patch5 -p1 -b .gennorm2-man.patch
%patch6 -p1 -b .icuinfo-man.patch
%patch7 -p1 -b .icu10143.memory.leak.crash.patch
%patch8 -p1 -b .icu10318.CVE-2013-2924_changeset_34076.patch
%patch9 -p1 -b .icurhbz1074549.CVE-2013-5907.patch

# http://userguide.icu-project.org/datetime/timezone#TOC-Updating-the-Time-Zone-Data
# says:
#
# > ICU4C TZ update when ICU data is built into a shared library
# > [...]
# > Copy the downloaded .txt files into the ICU sources for your installation,
# > in the subdirectory  source/data/misc/
# > [...]
cp %{SOURCE10} source/data/misc/
cp %{SOURCE11} source/data/misc/
cp %{SOURCE12} source/data/misc/
cp %{SOURCE13} source/data/misc/

chmod 644 source/i18n/unicode/selfmt.h

%build
cd source
autoconf
CFLAGS='%optflags -fno-strict-aliasing'
CXXFLAGS='%optflags -fno-strict-aliasing'
# Endian: BE=0 LE=1
%if ! 0%{?endian}
CPPFLAGS='-DU_IS_BIG_ENDIAN=1'
%endif
#rhbz856594 do not use --disable-renaming or cope with the mess
%configure --with-data-packaging=library --disable-samples
#rhbz#225896
sed -i 's|-nodefaultlibs -nostdlib||' config/mh-linux
#rhbz#681941
sed -i 's|^LIBS =.*|LIBS = -L../lib -licuuc -lpthread -lm|' i18n/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -licui18n -lc -lgcc|' io/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -lc|' layout/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -licule -lc|' layoutex/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../../lib -licutu -licuuc -lc|' tools/ctestfw/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../../lib -licui18n -licuuc -lpthread -lc|' tools/toolutil/Makefile
#rhbz#813484
sed -i 's| \$(docfilesdir)/installdox||' Makefile
# There is no source/doc/html/search/ directory
sed -i '/^\s\+\$(INSTALL_DATA) \$(docsrchfiles) \$(DESTDIR)\$(docdir)\/\$(docsubsrchdir)\s*$/d' Makefile
# rhbz#856594 The configure --disable-renaming and possibly other options
# result in icu/source/uconfig.h.prepend being created, include that content in
# icu/source/common/unicode/uconfig.h to propagate to consumer packages.
test -f uconfig.h.prepend && sed -e '/^#define __UCONFIG_H__/ r uconfig.h.prepend' -i common/unicode/uconfig.h

make %{?_smp_mflags}
make %{?_smp_mflags} doc

# remove the original timezone data and build the new data from the updated
# zoneinfo64.txt file:
%ifarch s390 s390x ppc ppc64
rm -f ./data/out/build/icudt50b/zoneinfo64.res
make -C data ./out/build/icudt50b/zoneinfo64.res
%else
rm -f ./data/out/build/icudt50l/zoneinfo64.res
make -C data ./out/build/icudt50l/zoneinfo64.res
%endif
make

%install
make %{?_smp_mflags} -C source install DESTDIR=$RPM_BUILD_ROOT
chmod +x $RPM_BUILD_ROOT%{_libdir}/*.so.*

# Remove files that aren't needed for the compat package
rm -rf $RPM_BUILD_ROOT%{_bindir}
rm -rf $RPM_BUILD_ROOT%{_includedir}
rm -rf $RPM_BUILD_ROOT%{_libdir}/*.so
rm -rf $RPM_BUILD_ROOT%{_libdir}/icu/
rm -rf $RPM_BUILD_ROOT%{_libdir}/pkgconfig/
rm -rf $RPM_BUILD_ROOT%{_sbindir}
rm -rf $RPM_BUILD_ROOT%{_datadir}/icu/
rm -rf $RPM_BUILD_ROOT%{_mandir}


%files
%{_libdir}/*.so.*


%changelog
* Wed Mar 12 2020 Ben Morrice <ben.morrice@cern.ch> - 50.2-3
- fix Requires

* Wed Mar 11 2020 Ben Morrice <ben.morrice@cern.ch> - 50.2-2
- remove build id links from rpm

* Wed Mar 11 2020 Ben Morrice <ben.morrice@cern.ch> - 50.2-1
- Initial release

# compat-libicu50

* this repo is to build a compat package of libicu (version 50) from EL7 for EL8
* It is to be used for testing WinCCOA compatibility on EL8 with the same library version as EL7
